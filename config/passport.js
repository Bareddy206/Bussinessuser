// const AnonymousStrategy = require('passport-anonymous').Strategy;
const passport = require('passport');
var session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');

const config = require('../config/config');
const sendMail = require('../methods/email');
const Storename=require('../models/storename');
const encrypt = require('../methods/encrypt');

const mailgun = require('mailgun-js')({
	apiKey: config.API_KEY_MAILGUN,
	domain: config.MAILGUN_DOMAIN
});

// The empty object '{}' passed to User crud methods is an optional options object to control the response received from database
// Right now since we are not using it, it's an empty object.

passport.serializeUser((user, done) => {
	console.log('Serializing user...');
	return done(null, user.email);
});

passport.deserializeUser((email, done) => {
	console.log('Deserializing user...');
	User.selectTable('Merchant_details');
	User.getItem({
		email: email
	}, null, (err, user) => {
		if (err) {
			console.log('err: ', err);
			return done(err);
		} else {
			return done(null, user);
		}
	});
});

passport.use('local-signup', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, (req, email, password, done) => {
	// Store hash in your password DB. 
    const bodyParams = req.body; // here we extract the username and any other details passed by the user while registering
	var s=bodyParams.storeName;
	
    console.log("suma:"+ s);
    const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname
	const name = bodyParams.storeName;
	
	if (bodyParams.password !== bodyParams.cpassword) {
		req.flash('error', 'Passwords don\'t match!');
		req.session.save(() => {
			return done(null, false);
		});
	} else {
		Storename.selectTable('store_details');
        Storename.getItem(name, {}, (err, name) => {
		console.log(name, err);
		if (err) {
			return res.send(statusMsg.errorResponse(err))
        } 
        if (Object.keys(name).length === 0) {
			console.log('storename',name);
			const userEmail=bodyParams.email;
			User.selectTable('Merchant_details');
	        User.getItem(userEmail, {}, (err, user) => {
				//console.log(user, err);
				if (err) {
					return res.send(statusMsg.errorResponse(err))
				} if (Object.keys(user).length === 0) {
					//console.log('user',user);
					//res.send(user)
					const createCallback = (hashnewpwd) => {
						const verification_code = randomstring.generate();
						const putParams = {
							"email": userEmail,
							"username": bodyParams.username,
							"storeName": bodyParams.storeName,
							"password": hashnewpwd,
							"verification_code": verification_code,
							"verifiedornot": 'no',
						};
						console.log("Adding a new item...\n", putParams);
						User.createItem(putParams, {table:'Merchant_details', overwrite: false }, (err, user) => {
							if (err) {
								//res.send(statusMsg.errorResponse(err));
							req.flash('err', `${err.message}`);
							req.session.save(() => {
								return done(err);
							});
							} else {
								console.log('\nAdded\n', user);

								const params = {"storeName": bodyParams.storeName };
								Storename.createItem(params, {table:'store_details', overwrite: false }, (err, name) => {
									if (err) {
										//res.send(statusMsg.errorResponse(err));
										req.flash('err', `${err.message}`);
										req.session.save(() => {
											return done(err);
										});
									} else {
										console.log('\nAdded\n', name);
										
								const mailData = sendMail.sendVerificationMail(userEmail,bodyParams.storeName, verification_code,rootUrl);
								mailgun.messages().send(mailData, (err, body) => {
									if (err) {
                                        //res.send(statusMsg.errorResponse(err));
                                        console.log('err: ', err);
                                        req.flash('error', 'Failed!');
                                        req.session.save(() => {
                                            return done(err, false);
                                        });
										
									} else {
										console.log('Sent mail to your mailid successfully');
									    req.flash('success', 'Registered! Verify your mailid');
									    req.session.save(() => {
										return done(null, user);
									    });
									}
								});
									}
								});
							}
						});
					};
					//encrypt.generateSalt(res, bodyParams.password, createCallback);
					bcrypt.genSalt(10, (err, salt) => {
						if (err) {
							console.log(`\nmsg-1\n ${err.message}`);
							req.flash('error', `${err.message}`);
							req.session.save(() => {
								return done(err);
							});
							//res.send(statusMsg.errorResponse(err));
						} else {
							bcrypt.hash(bodyParams.password, salt, (err, hashnewpwd) => {
								if (err) {
									console.log(`\nmsg-2\n ${err.message}`);
									req.flash('error', `${err.message}`)
									req.session.save(() => {
										return done(err);
									});
									//res.send(statusMsg.errorResponse(err));
								} else {
									createCallback(hashnewpwd);
								}
							})
						}
					})
					
				}
				if (Object.keys(user).length > 0) {
				    req.flash('error','Emailid already registered!');
                    req.session.save(()=>{
                        return done(null, false);
					});
					
				}
	       })
		}
		if (Object.keys(name).length > 0) {
			req.flash('error','StoreName already registered!');
			req.session.save(() => {
				return done(null, false);
			});
		}
	})
	}
}));

passport.use('local-login', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, (req, email, password, done) => {
	User.selectTable('Merchant_details');
	User.getItem({
		email:email
	}, null, (err, user) => {
		console.log('useremail: ',email );
		if (err) {
			req.flash('error', `${err.message}`);
			req.session.save(() => {
				return done(err);
			})
		} else if (Object.keys(user).length === 0) {
			return done(null, false, {
				message: 'Incorrect user!'
			});
		} else {
			const hashPassword = user.password;
			bcrypt.compare(password, hashPassword, (err, result) => {
				if (err) {
					console.log('err: ', err);
					req.flash('error', `${err.message}`);
					req.session.save(() => {
						return done(err)
					});
				}
				if (result) {
					console.log('\npassword matches', result);
					req.flash('success', 'Logged in successfully!');
					req.session.save(() => {
						return done(null,user);
					});
				} else {
					req.flash('error', 'Incorrect password!');
					req.session.save(() => {
						return done(null, false);
					});
				}
			})
		}

	})
}));

module.exports = passport